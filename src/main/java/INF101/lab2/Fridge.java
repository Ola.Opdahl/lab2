package INF101.lab2;

import java.util.ArrayList;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
    int capacity = 20;

    ArrayList<FridgeItem> fridgeItems;

    public Fridge() {
        fridgeItems = new ArrayList<>();
    }

    @Override
    public int totalSize() {
        return capacity;
    }

    @Override
    public int nItemsInFridge() {
        return this.fridgeItems.size();
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (this.fridgeItems.size() < this.capacity) {
            fridgeItems.add(item);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (fridgeItems.contains(item)) {
            fridgeItems.remove(item);
        } else {
            throw new NoSuchElementException();
        }
    }

    @Override
    public void emptyFridge() {
        if (fridgeItems.size() > 0) {
            this.fridgeItems.clear();
        } else {
            this.fridgeItems.clear();
        }

    }

    @Override
    public ArrayList<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> expiredFood = new ArrayList<>();
        for (int i = 0; i < fridgeItems.size(); i++) {
            if (fridgeItems.get(i).hasExpired()) {
                expiredFood.add(fridgeItems.remove(i));
                i--;
            }
        }

        return expiredFood;
    }

}
